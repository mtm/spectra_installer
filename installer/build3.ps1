Copy-Item -Force product3.wxs.tmpl product3.wxs

MkDir -Force Work3 | Out-Null

mkdir -Force Work3/Common/Plug-ins/7.0/MediaCore/ | Out-Null
mkdir -Force Output/ | Out-Null

Copy-Item ../app/Spectra.prm Work3/Common/Plug-ins/7.0/MediaCore/

$guid1 = (new-guid).guid
$guid1 = '014F5EB3-93B7-4B88-B5E6-3870E9E9FE33'
$guid2 = (new-guid).guid

(Get-Content product3.wxs) `
  -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="MyFeatureTitleName"' `
  -replace 'PUT-PRODUCT-NAME-HERE', 'AdobePluginForSpectra' `
  -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
  -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
  -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path product3.wxs

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Work3
giftmaster --very-verbose $files

heat dir Work3 -out components3.wxs `
  -cg ComponentGroupAdobe `
  -dr ADOBEROOTDIRECTORY `
  -var var.SourceFilesDir `
  -g1 `
  -ag `
  -srd `
  -suid `
  -sreg `
  -nologo `
  -arch x64

candle -out ./components3.wixobj components3.wxs `
  -ext WixUtilExtension `
  -dSourceFilesDir=Work3 `
  -nologo `
  -arch x64

candle -out ./product3.wixobj product3.wxs `
  -ext WixUtilExtension `
  -nologo `
  -arch x64
 
light components3.wixobj product3.wixobj -out Output/product3.msi `
  -ext WixUtilExtension `
  -ext WixUIExtension `
  -nologo `
  -spdb

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Output,Output1,Output0
giftmaster --very-verbose $files

