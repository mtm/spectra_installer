[CmdletBinding()]
Param (
    [Parameter(Mandatory = $true)][string]$Version,
    [Parameter(Mandatory = $true)][string]$OutDir,
    [Parameter(Mandatory = $true)][string]$MySku
)
$ErrorActionPreference = "Stop"

$install_dir = $pwd

mkdir -Force $OutDir | Out-Null

Copy-Item -Force product1.wxs.tmpl product1.wxs

MkDir -Force Work1 | Out-Null

MkDir -Force Work1/dist/ | Out-Null

#echo $env:STREAMBOX_CODE_SIGNING_PFX_PASSWORD | Out-File -Encoding ASCII c:/streambox-code-signing-password.txt
saltgang config --ini --yaml-path .\encassist.yml | Out-File -Encoding ASCII Work1/dist/encassist.ini
saltgang config --ini-check --yaml-path .\encassist.yml | Out-File -Encoding ASCII test_set_defaults_consume.py

$orig = $pwd
$root=git rev-parse --show-toplevel
$gobuild = "$root/installer/Resources/defaults_go"
saltgang config --go --yaml-path $root/installer/encassist.yml | Out-File -Encoding ASCII $gobuild/set_defaults.go
cd $gobuild
go mod download
go fmt set_defaults.go
go build set_defaults.go
Copy-Item "$gobuild/set_defaults.exe" "$root/installer/Work1/dist/set_defaults.exe"
cd $orig

unix2dos --quiet --force --newfile ../app/encassist.ini Work1/dist/encassist.ini
pytest --verbose tests/test_set_defaults_create.py
if ($LASTEXITCODE -ne 0) { throw "pytest can't work with encassist.ini, maybe its format is not valid?" }

$url = [System.Uri]'https://nssm.cc/ci/nssm-2.24-103-gdee49fc.zip'
$zip = Split-Path -Leaf -Path $url.AbsolutePath
$wc = New-Object System.Net.WebClient
$zip = Split-Path -Leaf -Path $url.AbsolutePath
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Write-Host "Downloading $($url.OriginalString)"
if (!(Test-Path($zip))) { $wc.DownloadFile($url.OriginalString, $zip) }
$basename = (Get-Item $zip).BaseName
$nssm = "$basename/win64/nssm.exe"
if (!(Test-Path($nssm))) { Expand-7Zip -ArchiveFileName $zip -TargetPath . }

Copy-Item ../app/chromactivate.exe                Work1/dist/
Copy-Item ../app/SpectraControlPanel.exe          Work1/dist/

mkdir -Force Work1/dist/curl | Out-Null
Copy-Item ../app/curl/msvcr90.dll                 Work1/dist/curl
Copy-Item ../app/curl/libssl32.dll                Work1/dist/curl
Copy-Item ../app/curl/libeay32.dll                Work1/dist/curl
Copy-Item ../app/curl/curl.exe                    Work1/dist/curl

Copy-Item disable-blackmagic.ps1                  Work1/
Copy-Item ../app/Processing.NDI.Lib.x64.dll       Work1/
Copy-Item ../app/sbxcmd.exe                       Work1/
Copy-Item ../app/Encoder3.exe                     Work1/
Copy-Item ../app/Encoder5.exe                     Work1/
Copy-Item control.ps1                             Work1/
Copy-Item control.psm1                            Work1/
Copy-Item "$basename/win64/nssm.exe"              Work1/
New-Item -Name Work1/service.log -ItemType File -Force | Out-Null
New-Item -Name Work1/service-error.log -ItemType File -Force | Out-Null

$guid1 = (new-guid).guid
$guid1 = '2B761967-D704-4C2B-9D13-4FB8DB07397C'
$guid2 = (new-guid).guid

(Get-Content product1.wxs) `
    -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="MyFeatureTitleName"' `
    -replace 'PUT-PRODUCT-VERSION-HERE', $Version `
    -replace 'PUT-PRODUCT-NAME-HERE', 'Spectra' `
    -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
    -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
    -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path product1.wxs

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Work1
giftmaster --very-verbose $files

heat dir Work1 -out components1.wxs `
    -dVersion $Version `
    -cg ComponentGroupSpectra `
    -dr SPECTRAROOTDIRECTORY `
    -var var.SourceFilesDir `
    -ag `
    -g1 `
    -srd `
    -suid `
    -sreg `
    -nologo `
    -arch x64

candle -out ./custom_action.wixobj custom_action.wxs `
    -ext WixUtilExtension `
    -nologo `
    -arch x64

candle -out ./components1.wixobj components1.wxs `
    -ext WixUtilExtension `
    -dSourceFilesDir=Work1 `
    -nologo `
    -arch x64

candle -out ./product1.wixobj product1.wxs `
    -ext WixUtilExtension `
    -dSourceFilesDir=Work1 `
    -dMySku="$MySku" `
    -nologo `
    -arch x64

light -out "$OutDir/spectra.msi" product1.wixobj components1.wixobj components2.wixobj components3.wixobj components4.wixobj components5.wixobj custom_action.wixobj `
    -ext WixUtilExtension `
    -ext WixUIExtension `
    -dMySku="$MySku" `
    -spdb `
    -nologo

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Output,Output1,Output0
giftmaster --very-verbose $files
