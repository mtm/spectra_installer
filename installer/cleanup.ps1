$artifacts = @(
  'Work',
  'OutputWixBurnEngine',
  'Work1',
  'output-universal-quickstart',
  'output-repackage-quickstart',
  'output-extract-quickstart',
  'output-quickstart-guides',
  'output-quickstart',
  'output-macos-quickstart',
  'Work2',
  'Work3',
  'Work4',
  'Output',
  'Output0',
  'Output1',
  'venv',
  'control.ps1',
  'control.psm1',
  'spectra.zip',
  'testResults.xml',
  '*.wxs',
  '*.log',
  '*.lf',
  '*.wixobj'
)

foreach ($artifact in $artifacts){
    $path = Join-Path $pwd $artifact
    echo $path
    if(Test-Path($path)){
        Remove-Item -Force -Recurse $path
    }
}
