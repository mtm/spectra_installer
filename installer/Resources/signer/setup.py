import setuptools

setuptools.setup(
    name="sbx-spectra-sign",
    version="0.0.1",
    python_requires=">=3.7",
    packages=setuptools.find_packages(),
    install_requires=[
        "binaryornot",
    ],
    py_modules=[
        "sign",
    ],
    entry_points={
        "console_scripts": [],
    },
)
