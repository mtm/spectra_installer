import setuptools

setuptools.setup(
    name="sbx-spectra-signit",
    version="0.0.1",
    python_requires=">=3.7",
    packages=setuptools.find_packages(),
    install_requires=[
        "binaryornot",
        "monacelli_pylog_prefs",
    ],
    py_modules=[
        "signit",
    ],
    entry_points={
        "console_scripts": [
            "signit=signit.main:main",
        ],
    },
)
