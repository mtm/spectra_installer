import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--folders", nargs="+", required=True)
parser.add_argument(
    "-ns",
    "--no-signing-sleep",
    default=False,
    help="speed up debugging signing",
    action="store_true",
)
parser.add_argument(
    "-b", "--bundle", default=False, help="sign bundle", action="store_true"
)
args = parser.parse_args()
