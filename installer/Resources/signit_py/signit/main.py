import datetime
import enum
import logging
import os
import pathlib
import random
import subprocess
import time

import binaryornot.check
import monacelli_pylog_prefs.logger

from signit import myargs, pathfromglob

monacelli_pylog_prefs.logger.setup(
    filename=f"{pathlib.Path(__file__).stem}.log", stream_level=logging.DEBUG
)

args = myargs.parser.parse_args()

pfx_pass = os.environ.get("STREAMBOX_CODE_SIGNING_PFX_PASSWORD", None)

HASH_ALGORITHM = "SHA256"

pause_between_signs = datetime.timedelta(minutes=0, seconds=30)
if args.no_signing_sleep:
    pause_between_signs = datetime.timedelta(minutes=0, seconds=0)
px86 = pathlib.Path("C:/Program Files (x86)")
pfx_path = pathlib.Path("C:/streambox-code-signing.pfx")
installer_path = pathlib.Path.cwd()
engine_basedir = installer_path / "OutputWixBurnEngine"
insignia_path = pathfromglob.PathFromGlob.from_string(
    "C:/Program*/WiX Toolset*/bin/insignia.exe"
).path
signtool_path = pathfromglob.PathFromGlob.from_string(
    """
    C:/Program Files*/Microsoft SDKs/*/SignTool/signtool.exe
    C:/Program*/Microsoft SDKs/Windows/v[!6]*/Bin/SignTool.exe
    C:/Program*/Windows Kits/*/bin/*/*/SignTool.exe
"""
).path


class Status(enum.Enum):
    OK = 1
    FAIL = 2


def is_binary(path):
    if not path.is_file():
        return False

    if binaryornot.check.is_binary(str(path)):
        return True

    return False


def doit(cmd, check_error=None, check_success=None):
    logging.debug(" ".join(cmd))

    try:
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = process.communicate()
    except BaseException as ex:
        logging.exception(ex)
        raise

    if stderr.decode():
        logging.debug(stderr.decode().strip())

    if check_error:
        if check_error in stderr.decode():
            return Status.FAIL

    if check_success:
        if check_success in stdout.decode():
            return Status.OK
        return Status.FAIL


def backoff(timedelta: datetime.timedelta = pause_between_signs):
    if timedelta.total_seconds() == 0:
        return

    if not args.no_signing_sleep:
        logging.debug(
            (
                f"sleeping {timedelta} to be respectful of request "
                "from https://sectigo.com/resource-library/time-stamping-server"
            )
        )
        time.sleep(timedelta.total_seconds())


def sign(path):
    if is_signed(path):
        logging.debug(f"SIGNED:{path.name}")

    else:
        tries = 1
        while tries < 10 and not is_signed(path):
            timestamp_url = random.choice(
                [
                    "http://timestamp.comodoca.com/rfc3161",
                    "http://timestamp.digicert.com",
                    "http://timestamp.globalsign.com/scripts/timestamp.dll",
                    "http://timestamp.sectigo.com",
                    "http://tsa.starfieldtech.com",
                ]
            )

            # https://docs.microsoft.com/en-us/windows/win32/seccrypto/signtool#timestamp-command-options
            cmd = [
                str(signtool_path),
                "sign",
                "/v",
                "/n",
                "streambox",
                "/s",
                "My",
                "/fd",
                HASH_ALGORITHM,
                "/d",
                "spectra",
                "/tr",
                timestamp_url,
                "/td",
                HASH_ALGORITHM,
                str(path),
            ]

            doit(cmd)
            # try not to hit timestamp quota
            backoff(pause_between_signs)
            tries += 1

        if not is_signed(path):
            logging.warning(f"NOT SIGNED:{path.name}")
        else:
            logging.debug(f"SIGNED:{path.name}")


def sign_bundle(vendor_name):
    """
    https://wixtoolset.org/documentation/manual/v3/overview/insignia.html

    insignia -ib bundle.exe -o engine.exe
    ... sign engine.exe
    insignia -ab engine.exe bundle.exe -o bundle.exe
    ... sign bundle.exe
    """

    bundle_path = installer_path / f"Output0/{vendor_name}/spectra.exe"
    engine_path = engine_basedir / f"{vendor_name}/engine.exe"

    engine_path.parent.mkdir(parents=True, exist_ok=True)

    # insignia -ib bundle.exe -o engine.exe
    cmd = [
        str(insignia_path),
        "-ib",
        str(bundle_path),
        "-o",
        str(engine_path),
    ]
    doit(cmd)

    # ... sign engine.exe
    sign(engine_path)

    # insignia -ab engine.exe bundle.exe -o bundle.exe
    cmd = [
        str(insignia_path),
        "-ab",
        str(engine_path),
        str(bundle_path),
        "-o",
        str(bundle_path),
    ]
    doit(cmd)

    # ... sign bundle.exe
    sign(bundle_path)


def is_signed(path):

    # C:\spectra_installer\installer\Work1\Encoder3.exe -> signing_log_spectra_installer-installer-Work1-Encoder3.exe.log
    t1 = str(path)
    t1 = t1.replace(path.drive, "")
    t1 = t1.replace("\\", "-")
    t1 = f"signing_log_{t1}.log"
    sign_log_path = installer_path / t1

    # https://docs.microsoft.com/en-us/windows/win32/seccrypto/signtool#verify-command-options
    cmd = [
        str(signtool_path),
        "verify",
        "/a",
        "/v",
        "/hash",
        HASH_ALGORITHM,
        "/tw",
        "/pa",
        "/ph",
        str(path),
    ]

    logging.debug(" ".join(cmd))

    try:
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = process.communicate()
    except BaseException as ex:
        logging.exception(ex)
        raise

    sign_log_path.write_text(stdout.decode())

    if "Number of files successfully Verified: 1" in stdout.decode():
        return True

    err = stderr.decode()

    if "SignTool Error:" in err or "No signature found" in err:
        return False

    return False


def do_most_stuff(folders):
    pass_path = pathlib.Path(r"c:/streambox-code-signing-password.txt")

    global pfx_pass
    if not pfx_pass:
        pfx_pass = pass_path.read_text().strip()
        if not pfx_pass:
            logging.warning("pfx_pass is None")

    if not pfx_path:
        logging.warning("pfx_path is None")

    for path_str in folders:
        path = pathlib.Path(path_str)

        if not path.exists():
            continue

        for path_to_sign in path.glob("**/*"):
            path_to_sign = path_to_sign.resolve()

            if not path_to_sign.is_file():
                continue

            # sign powershell files even though they're not binary
            if not is_binary(path_to_sign) and not path_to_sign.suffix.lower() in [
                ".psm1",
                ".ps1",
            ]:
                logging.debug(f"skip signing {path_to_sign.name}, its not binary")
                continue

            if path_to_sign.name in ["spectra.exe"]:
                # sign bundles differently, eg. sign_bundle("Avid")
                logging.debug(
                    f"skip signing {path_to_sign.name} because it should be signed after insignia"
                )
                continue

            if is_signed(path_to_sign):
                logging.debug(f"SIGNED:{path_to_sign.name}")
                continue

            sign(path_to_sign)


def main():
    if args.bundle:
        sign_bundle("Avid")
        sign_bundle("Universal")

    else:
        do_most_stuff(args.folders)


if __name__ == "__main__":
    main()
