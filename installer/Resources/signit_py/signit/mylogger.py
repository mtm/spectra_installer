import logging

filename = f"{__name__.split('.')[0]}.log"

format_fh = "[%(asctime)s] p%(process)s {%(filename)s:%(lineno)d} %(levelname)s - %(message)s"  # file
format_ch = "{%(filename)s:%(lineno)d} %(levelname)s - %(message)s"  # console

formatter_fh = logging.Formatter(format_fh, "%Y-%m-%d %H:%M:%S %Z")  # add timezone
formatter_ch = logging.Formatter(format_ch)

fh = logging.FileHandler(filename)
ch = logging.StreamHandler()

# fh.setLevel(logging.DEBUG)
ch.setLevel(logging.INFO)

fh.setFormatter(formatter_fh)
ch.setFormatter(formatter_ch)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logger.addHandler(fh)
logger.addHandler(ch)
