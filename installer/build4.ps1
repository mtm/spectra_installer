Copy-Item -Force product4.wxs.tmpl product4.wxs

Get-ChildItem Work4 -Recurse | Remove-Item -Force -Recurse

MkDir -Force Work4 | Out-Null
MkDir -Force Output/ | Out-Null

MkDir -Force Work4/SpectraUI | Out-Null
MkDir -Force Work4/SpectraUI/hash | Out-Null

unix2dos --quiet --force --newfile ../app/settings.xml Work4/SpectraUI/settings.xml
unix2dos --quiet --force --newfile ../app/settings.xml Work4/SpectraUI/settings.xml.bak
unix2dos --quiet --force --newfile ../app/liveServers.xml Work4/SpectraUI/liveServers.xml

$version = initool g ../.bumpversion.cfg bumpversion current_version --value-only
Set-Content -Encoding ASCII -Path Work4/SpectraUI/version.txt -Value $version

$guid1 = (new-guid).guid
$guid1 = '4F09CB52-5D12-11EB-9C06-30D9D9EED5CB'
$guid2 = (new-guid).guid

(Get-Content product4.wxs) `
  -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="SpectraProgramDataFiles"' `
  -replace 'PUT-PRODUCT-NAME-HERE', 'SpectraProgramDataFiles' `
  -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
  -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
  -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path product4.wxs

heat dir Work4 -out components4.wxs `
  -cg ComponentGroupProgramData `
  -dr SPECTRADATAROOTDIR `
  -var var.SourceFilesDir `
  -g1 `
  -ag `
  -srd `
  -suid `
  -sreg `
  -nologo `
  -arch x64

candle -out ./components4.wixobj components4.wxs `
  -ext WixUtilExtension `
  -dSourceFilesDir=Work4 `
  -nologo `
  -arch x64

candle -out ./product4.wixobj product4.wxs `
  -ext WixUtilExtension `
  -nologo `
  -arch x64

light components4.wixobj product4.wixobj -out Output/product4.msi `
  -ext WixUtilExtension `
  -ext WixUIExtension `
  -spdb `
  -nologo

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Output,Output1,Output0
giftmaster --very-verbose $files

