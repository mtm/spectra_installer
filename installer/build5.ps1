Copy-Item -Force product5.wxs.tmpl product5.wxs

Get-ChildItem Work5 -Recurse | Remove-Item -Force -Recurse

New-Item -Force -type "directory" Work5/ | Out-Null
New-Item -Force -type "directory" Output/ | Out-Null

<# goal:
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Resources\com.Streambox.Spectra.png
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx
#>

New-Item -Force -type "directory" 'Work5/OFX/Plugins' | Out-Null
Copy-Item -Force -Recurse ../app/SpectraPlugin.ofx.bundle 'Work5/OFX/Plugins/SpectraPlugin.ofx.bundle'

$version = initool g ../.bumpversion.cfg bumpversion current_version --value-only

$guid1 = '245D9D50-8A0A-11EB-BBED-30D9D9EED5CB'
$guid2 = (new-guid).guid

(Get-Content product5.wxs) `
  -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="SpectraOpenFX"' `
  -replace 'PUT-PRODUCT-NAME-HERE', 'SpectraOpenFX' `
  -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
  -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
  -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path product5.wxs

heat dir Work5 -out components5.wxs `
  -cg ComponentGroupOpenFX `
  -dr SPECTRAOPENFXROOTDIR `
  -var var.SourceFilesDir `
  -g1 `
  -ag `
  -srd `
  -suid `
  -sreg `
  -nologo `
  -arch x64

candle -out ./components5.wixobj components5.wxs `
  -ext WixUtilExtension `
  -dSourceFilesDir=Work5 `
  -nologo `
  -arch x64

candle -out ./product5.wixobj product5.wxs `
  -ext WixUtilExtension `
  -nologo `
  -arch x64

light components5.wixobj product5.wixobj -out Output/product5.msi `
  -ext WixUtilExtension `
  -ext WixUIExtension `
  -spdb `
  -nologo

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Output,Output1,Output0
giftmaster --very-verbose $files

