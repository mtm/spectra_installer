Copy-Item -Force product2.wxs.tmpl product2.wxs

MkDir -Force Work2 | Out-Null

mkdir -Force Work2/AVX2_Plug-ins/ | Out-Null
mkdir -Force Output/ | Out-Null

Copy-Item ../app/Spectra.acf Work2/AVX2_Plug-ins/

$guid1 = (new-guid).guid
$guid1 = '5D08CAA4-8A4B-451E-8C16-814A2E662D1D'
$guid2 = (new-guid).guid

(Get-Content product2.wxs) `
  -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="AvidPluginForSpectra"' `
  -replace 'PUT-PRODUCT-NAME-HERE', 'AvidPluginForSpectra' `
  -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
  -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
  -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path product2.wxs

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Work2
giftmaster --very-verbose $files

heat dir Work2 -out components2.wxs `
  -cg ComponentGroupAvid `
  -dr AVIDROOTDIRECTORY `
  -var var.SourceFilesDir `
  -g1 `
  -ag `
  -srd `
  -suid `
  -sreg `
  -nologo `
  -arch x64

candle -out ./components2.wixobj components2.wxs `
  -ext WixUtilExtension `
  -dSourceFilesDir=Work2 `
  -nologo `
  -arch x64

candle -out ./product2.wixobj product2.wxs `
  -ext WixUtilExtension `
  -nologo `
  -arch x64

light components2.wixobj product2.wixobj -out Output/product2.msi `
  -ext WixUtilExtension `
  -ext WixUIExtension `
  -spdb `
  -nologo

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Output,Output1,Output0
giftmaster --very-verbose $files
