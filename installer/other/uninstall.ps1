pwsh -c '$pester = Invoke-Pester -PassThru -Script tests\All.Tests.ps1 -Verbose -Show All -Name UninstallService'

$obj = Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | 
    Where-Object {$_.DisplayName -eq 'Streambox Spectra' -or $_.DisplayName -eq 'Spectra'}

if($obj){
     $cmd = $obj.QuietUninstallString
     iex "& $cmd" | Out-String
}
