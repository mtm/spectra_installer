# https://stackoverflow.com/a/50065953/1495086

# Read current path
function Get-ScriptDirectory {
    $Invocation = (Get-Variable MyInvocation -Scope 1).Value
    Split-Path $Invocation.MyCommand.Path
}

$installpath = Get-ScriptDirectory
$RunScript = "Test.ps1"
$ShCutLnk = "PwSh $RunScript.lnk"

# create shortcut
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$installpath\$ShCutLnk")
$Shortcut.TargetPath = "C:\Windows\System32\windowspowershell\v1.0\powershell.exe"
$Shortcut.IconLocation = "C:\Windows\System32\windowspowershell\v1.0\powershell.exe,0" # icon index 0
$Shortcut.Arguments = "-nop -Executionpolicy bypass -NoExit ""$installPath\$RunScript"""
$Shortcut.WorkingDirectory = "$installpath"
$Shortcut.Save()

## Make the Shortcut runas Administrator
## Source: https://stackoverflow.com/questions/28997799/how-to-create-a-run-as-administrator-shortcut-using-powershell
$bytes = [System.IO.File]::ReadAllBytes("$installpath\$ShCutLnk")
$bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
[System.IO.File]::WriteAllBytes("$installpath\$ShCutLnk", $bytes)
