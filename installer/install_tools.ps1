$PSVersionTable.PSVersion

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12


if($env:APPVEYOR_BUILD_FOLDER -eq $null)
{
    # appveyor has this stuff already

    .\install_chocolatey.ps1
    choco install awscli
    choco install dos2unix
    choco install git
    choco install python
    choco install wixtoolset
    choco install git
    choco install golang
}

choco install make
choco install pdfcpu
choco install yq
choco install ytt
choco install wget
choco install vscode
choco install html-tidy

choco install initool
If($PSVersionTable.PSEdition -eq "Desktop") { Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force; }
Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
Install-Module -Name 7Zip4Powershell
Install-Module -SkipPublisherCheck -Force -Name Pester 

Find-Module TUN.Logging
Install-Module TUN.Logging

choco install windows-sdk-10.1 --override-arguments --install-args="'/quiet /features Optionid.MSIInstallTools Optionid.SigningTools'"
$signtool = Get-ChildItem -Recurse "C:\Program*\Windows Kits\10\*\*\x64" -Filter "signtool.exe" | select-object -first 1
$CurrentValue = [Environment]::GetEnvironmentVariable("PATH", "User")
[Environment]::SetEnvironmentVariable("PATH", $CurrentValue + ";$($signtool.Directory.Fullname)", "User")
Import-Module -MinimumVersion 5.1.1 -Force Pester
$signtool | Should -Not -BeNullOrEmpty
