Copy-Item -Force product10.wxs.tmpl product10.wxs

mkdir -Force Work10/ | Out-Null
mkdir -Force Output/ | Out-Null

$guid1 = (new-guid).guid
$guid1 = 'FDE2620E-3435-4488-A3F5-AED939FF93C0'
$guid2 = (new-guid).guid

(Get-Content product10.wxs) `
  -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="MyFeatureTitleName"' `
  -replace 'PUT-PRODUCT-NAME-HERE', 'Spectra' `
  -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
  -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
  -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path product10.wxs

heat dir Work10 -out product10.wxs `
  -nologo `
  -var var.SourceFilesDir `
  -g1 `
  -ag `
  -srd `
  -sreg `
  -out components4.wxs `
  -cg ComponentGroupSpectra `
  -dr SPECTRAROOTDIRECTORY

candle -out ./product10.wixobj product10.wxs `
  -nologo `
  -ext WixUtilExtension 

light -out Output/product10.msi `
  -spdb `
  -nologo `
  -ext WixUtilExtension `
  -ext WixUIExtension `
  product10.wixobj
