Import-Module -MinimumVersion 5.1.1 -Force Pester

$rootStore = Get-Item Cert:\CurrentUser\My
$rootStore.Open("ReadWrite")
$rootStore.Add($cert)
$rootStore.Close()

$cert = Get-ChildItem Cert:\CurrentUser\My |
   Where-Object { $_.Subject -match "Streambox" } |
   Select -First 1

$cert | Should -Not -BeNullOrEmpty

$certs = Get-ChildItem Cert:\CurrentUser\My |
Where-Object { $_.Subject -match "Streambox" }

ForEach($cert in $certs)
{
    $expire = $cert.NotAfter
    Write-Host "Cert expires in $(($expire - (Get-Date)).Days) days on $expire"
}

ForEach($cert in $certs)
{
    if((Get-Date) -gt $cert.NotAfter) {
        throw "Certificate $cert expired"
    }
}
