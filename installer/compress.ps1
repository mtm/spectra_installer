Import-Module 7Zip4Powershell

$basedir = $pwd
$zip = Join-Path $basedir spectra.zip

unix2dos --quiet --force --newfile ../changelog-win.yml Output0/changelog.yml

cd Output0
Get-ChildItem . -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Select-Object -ExpandProperty fullname |
    Compress-7Zip -CompressionLevel Ultra -ArchiveFileName $zip
