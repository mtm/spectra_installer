Set-ExecutionPolicy -Force bypass
$PSVersionTable.PSVersion
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$nuget = Get-PackageProvider -Force -ErrorAction SilentlyContinue -Name NuGet
if(!$nuget){ Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force }

$psgallery = Get-PSRepository -ErrorAction SilentlyContinue -Name PSGallery
if(!$psgallery.Trusted){ Set-PSRepository -Verbose -Name PSGallery -InstallationPolicy Trusted }

if (!(Get-Module -ListAvailable -Name 7Zip4Powershell)) { Install-Module -Verbose -Name 7Zip4Powershell }

$pester = Get-InstalledModule -ErrorAction SilentlyContinue -Name Pester 
if(!$pester) { Install-Module -Name Pester -SkipPublisherCheck -Force } 

$pester = Get-InstalledModule -ErrorAction SilentlyContinue -Name Pester 
if($pester.Version -lt [version]'5.0.2') {
    Install-Module -Name Pester -SkipPublisherCheck -Force
}

Import-Module 7Zip4Powershell
Import-Module Pester -MinimumVersion 5.0.2

Function Just-ForTest(){
    $url = 'https://streambox-spectra.s3-us-west-2.amazonaws.com/0.9.129.0/avid/spectra.exe'
    $url = [System.Uri]$url

    MkDir -Force Output0/Avid | Out-Null

    $wc = New-Object System.Net.WebClient
    $filename = Split-Path -Leaf -Path $url.AbsolutePath
    $extract_to = Join-Path Output0/Avid $filename
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    if (!(Test-Path($extract_to))) { $wc.DownloadFile($url.OriginalString, $extract_to) }
}

$sandbox_dir = 'output-repackage-quickstart'

MkDir -Force $sandbox_dir | Out-Null
Get-ChildItem $sandbox_dir/step2 -Recurse | Remove-Item -Force
Get-ChildItem $sandbox_dir/step3 -Recurse | Remove-Item -Force
Get-ChildItem $sandbox_dir/step4_verify -Recurse | Remove-Item -Force

MkDir -Force $sandbox_dir/step1 | Out-Null
MkDir -Force $sandbox_dir/step2 | Out-Null
MkDir -Force $sandbox_dir/step3 | Out-Null

$url = 'https://www.dropbox.com/s/i2zivcwp8xp99jj/Streambox_Spectra_for_Avid%20Media_Composer.zip?dl=1'
$url = [System.Uri]$url
$decoded_name = Split-Path -Leaf $url.LocalPath # decode pesky %20
$reconst_zip = Join-Path (pwd) "$sandbox_dir/step3/$decoded_name"

$wc = New-Object System.Net.WebClient
$filename = Split-Path -Leaf -Path $url.AbsolutePath
$extract_to = Join-Path $sandbox_dir $filename
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
if (!(Test-Path($extract_to))) { $wc.DownloadFile($url.OriginalString, $extract_to) }
Expand-7Zip -ArchiveFileName $extract_to -TargetPath $sandbox_dir/step1
$pdf_path = Join-Path $sandbox_dir/step1 "Streambox_Spectra_for_Avid Media_Composer-RR2.pdf"

# Just-ForTest

Copy-Item -Force Output0/Avid/spectra.exe $sandbox_dir/step2
Copy-Item -Force $pdf_path $sandbox_dir/step2

$cdir = (pwd)
Set-Location $sandbox_dir/step2
dir * | Compress-7Zip -Verbose -CompressionLevel Ultra -ArchiveFileName $reconst_zip
Set-Location $cdir

# verify
MkDir -Force $sandbox_dir/step4_verify | Out-Null
Expand-7Zip -ArchiveFileName $reconst_zip -TargetPath $sandbox_dir/step4_verify

$pdf = Get-ChildItem -Recurse $sandbox_dir/step4_verify -Filter "*.pdf"
$spectra = Get-ChildItem -Recurse $sandbox_dir/step4_verify -Filter "spectra.exe"

$pdf.Count | Should -BeExactly 1
$spectra.Count | Should -BeExactly 1

$pdf.Length | Should -BeGreaterThan 1MB
$spectra.Length | Should -BeGreaterThan 23MB

# powershell Where-Object type directory
# Get-ChildItem list only file names exclude directories

# spot check
Get-ChildItem -Recurse $sandbox_dir | select -exp fullname
