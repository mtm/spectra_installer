Function Zip-Logs {
    Import-Module 7Zip4Powershell
    dir *.wxs, *.xml, *.log | Compress-7Zip -CompressionLevel Ultra -ArchiveFileName logs.7z
}

Function Get-BuildVersion {
    $root = git rev-parse --show-toplevel
    $bumpconfig = Join-Path $root .bumpversion.cfg
    $version = (Get-Content $bumpconfig | Select-String -Pattern 'current_version\s*=\s*([\d\.]+)' | % { $($_.matches.groups[1]) }).Value
    $version
}

Function s3_sync_setup {
    param(
        [Parameter(Mandatory = $true)][version]$version,
        [Parameter(Mandatory = $false)][string]$dir = "output-s3-upload", 
        [Parameter(Mandatory = $false)][bool]$dryrun = $false
    )
    Import-Module Pester -MinimumVersion 5.0.2

    Set-Content -Encoding ASCII -Path ./version.txt -Value $version

    $outdir = Join-Path $dir $version

    $map = @{ 
        "$outdir\win\avid\spectra.exe"                   = "Output0\Avid\spectra.exe"
        "$outdir\win\universal\spectra.exe"              = "Output0\Universal\spectra.exe"
        "$outdir\win\version.txt"                        = "version.txt"
        "$outdir\win\spectra.zip"                        = "spectra.zip"
        "$outdir\win\spectra_win_${version}.zip"         = "spectra.zip"
        "$outdir\win\changelog.yml"                      = "..\changelog-win.yml"

        # legacy - added before macos build was created:
        "$outdir\version.txt"                            = "version.txt"

# meh
#        "$outdir\win\avid\spectra_avid_${version}.exe"   = "Output0\Avid\spectra.exe"
#        "$outdir\win\universal\spectra_universal_${version}.exe" = "Output0\universal\spectra.exe"
#        "$outdir\win\spectra_win.zip"                    = "spectra.zip"

        # wait for test to complete
        "$outdir\logs.7z"                                = "logs.7z"

        # legacy hierarchy
        "$outdir\spectra_win.zip"                        = "spectra.zip"
        "$outdir\spectra.zip"                            = "spectra.zip"
        "$outdir\changelog.yml"                          = "..\changelog-win.yml"
        "$outdir\avid\spectra.exe"                       = "Output0\Avid\spectra.exe"
    }

    foreach ($elem in $map.GetEnumerator()) {
        $outdir = Split-Path -Parent $elem.Name
        If (!$dryrun) { 
            New-Item -Force -Type "directory" $outdir | Out-Null
            $outdir | Should -Exist
        }

        If (!$dryrun) {
            If (!(Test-Path($source))) {
                # defer to uploading test logs until later
                Write-HostLog "Skipping $source because it doesn't yet exist"
                continue
            }
        }

        $source = Join-Path $pwd $elem.Value

        If (!$dryrun) {
            $source | Should -Exist
        }

        $target = $elem.Name
        Write-HostLog "Copy $source $target"

        If (!$dryrun) {
            Copy-Item -Force $source $target
            $target | Should -Exist
        }
    }
}

Function Upload-Logs {
    Get-ChildItem logs.7z | % { Push-AppveyorArtifact $_.FullName -FileName logs.7z }
}

# DEBUG
# $source = Join-Path $pwd output-s3-upload; s3_sync_setup -dir $source -version 0.9.132.0 -dryrun $true; start output-s3-upload

Function Upload-Artifacts {
    Import-Module Pester -MinimumVersion 5.0.2
    Import-Module TUN.Logging -Force

    Start-Transcript -Append -Path env.log

    Start-Log -LogPath ".\" -UseComputerPrefix -UseScriptPrefix -LogName "yyyy-MM-dd"

    Try {
        # Get-ChildItem spectra.zip | % { Push-AppveyorArtifact $_.FullName -FileName spectra.zip }
        $source = Join-Path $pwd output-s3-upload

        s3_sync_setup -dir $source -version (Get-BuildVersion) -dryrun $false
        aws s3 sync $source s3://streambox-spectra --quiet --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --region us-west-2
    }
    Catch {
        $_ | Write-ErrorLog -NoOut "Encountered critical error"
    }
    finally {
        Stop-Log
    }
}

$env:APPVEYOR_SPECTRA_TESTS_PASSSED_SENTINEL = Join-Path $env:APPVEYOR_BUILD_FOLDER\installer\tests TESTS_PASSED

$wix_dir = (Get-ChildItem -Recurse C:\Program*\Wix*Toolset*\bin -Filter "heat.exe" | select-object -first 1).Directory.FullName
$env:PATH = "$wix_dir;$env:PATH"

$paths = @(
    "C:\Python312"
    "C:\Python312\Scripts"
    "C:\Python312-x64"
    "C:\Python312-x64\Scripts"

    "C:\Python311"
    "C:\Python311\Scripts"
    "C:\Python311-x64"
    "C:\Python311-x64\Scripts"

    "C:\Python310"
    "C:\Python310\Scripts"
    "C:\Python310-x64"
    "C:\Python310-x64\Scripts"

    "C:\Python39"
    "C:\Python39\Scripts"
    "C:\Python39-x64"
    "C:\Python39-x64\Scripts"

    "C:\Python38"
    "C:\Python38\Scripts"
    "C:\Python38-x64"
    "C:\Python38-x64\Scripts"

    "C:\Python37"
    "C:\Python37\Scripts"
    "C:\Python37-x64"
    "C:\Python37-x64\Scripts"

) -Join ';'
$env:PATH = "$paths;$env:PATH"

$env:PATH += ";C:\ProgramData\chocolatey\bin"
$env:PATH += ";C:\Program Files\Git\bin"
$env:PATH += ";C:\Program Files\Amazon\AWSCLIV2"
$env:PATH += ";C:\Go\bin"
$env:PATH += ";C:\go15-x86\bin"
