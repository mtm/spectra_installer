function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )
    if (-not (Test-Path $Destination)) {
        Write-Verbose "Downloading $Url"
        $wc.DownloadFile($Url, $Destination)
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

function pretty {
  Get-ChildItem .\* -include ("*.xml", "*.wxs", "*.txt", "*.log") |
   Select-Object -ExpandProperty fullname |
   ForEach-Object { dos2unix --quiet --force $_ }
}
