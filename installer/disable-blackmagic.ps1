$from = "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf"
$to = "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled"

If(Test-Path($from)) {
    Move-Item -ErrorAction SilentlyContinue -Force $from $to
}
