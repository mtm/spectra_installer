python -m venv venv

. .\venv\Scripts\activate.ps1

python -m pip install `
    --use-feature=in-tree-build `
    --disable-pip-version-check `
    --quiet `
    --upgrade `
    'giftmaster>=0.0.30' `
    'saltgang>=0.0.5' `
    pytest `
    pip 
