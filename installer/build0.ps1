using module .\common.psm1

[CmdletBinding()]
Param (
    [Parameter(Mandatory = $true)][String] $Version,
    [Parameter(Mandatory = $true)][String] $InDir,
    [Parameter(Mandatory = $true)][String] $OutDir
)
$ErrorActionPreference = "Stop"

$wc = New-Object System.Net.WebClient

Write-Verbose "Downloading VCRedist..."
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Get-FileIfNotExists "https://aka.ms/vs/16/release/vc_redist.x64.exe" "Resources\vc_redist.x64.exe"

Copy-Item -Force bundle0.wxs.tmpl bundle0.wxs

mkdir -Force $OutDir | Out-Null

$guid1 = (new-guid).guid
$guid1 = '5498EC65-276D-4900-8416-C901EA53EF63'
$guid2 = (new-guid).guid

(Get-Content bundle0.wxs) `
    -replace 'Title="PUT-FEATURE-TITLE-HERE"', 'Title="MyFeatureTitleName"' `
    -replace 'PUT-PRODUCT-VERSION-HERE', $Version `
    -replace 'PUT-PRODUCT-NAME-HERE', 'Spectra' `
    -replace 'PUT-COMPANY-NAME-HERE', 'Streambox' `
    -replace 'UpgradeCode="PUT-GUID-HERE"', "UpgradeCode=`"$guid1`"" `
    -replace 'Id="PUT-GUID-HERE"', "Id=`"$guid2`"" | Set-Content -Path bundle0.wxs
 
$msi = Join-Path -Path $InDir -ChildPath spectra.msi
$exe = Join-Path -Path $OutDir -ChildPath spectra.exe

candle bundle0.wxs -out bundle0.wixobj `
    -ext WixBalExtension `
    -ext WixUtilExtension `
    -arch x64 `
    -dMsiPath="$msi" `
    -nologo

light bundle0.wixobj -out $exe `
    -ext WixBalExtension `
    -spdb `
    -nologo

$dont_sign = @(
    '.bat',
    '.cfg',
    '.disabled',
    '.envrc',
    '.gitignore',
    '.go',
    '.ico',
    '.ini',
    '.log',
    '.md',
    '.mod',
    '.py',
    '.pyc',
    '.sum',
    '.txt',
    '.tmpl',
    '.wxl',
    '.wxs',
    '.xml',
    '.yml',
    'Makefile'
)

Function Get-FilesByExtension([string[]]$roots, [string[]]$ignore) {
    Get-ChildItem $roots -Recurse |
    Where-Object { ! $_.PSIsContainer } |
    Where-Object { 
        $ignore -NotContains $_.Extension
    } | Select-Object -Expand Fullname
}

$files=Get-FilesByExtension -ignore $dont_sign -roots Output0,Output,Output1
giftmaster --very-verbose $files
