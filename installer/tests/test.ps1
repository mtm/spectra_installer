Import-Module Pester -MinimumVersion 5.0.2

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here\config.ps1"
$PesterPreference = $pesterConfig

if ($env:APPVEYOR_JOB_ID) {
  if ($env:APPVEYOR_SPECTRA_TESTS_PASSSED_SENTINEL -and (Test-Path($env:APPVEYOR_SPECTRA_TESTS_PASSSED_SENTINEL))) {
    Remove-Item -Force $env:APPVEYOR_SPECTRA_TESTS_PASSSED_SENTINEL
  }
}

Try {
  $result = Invoke-Pester
}
Catch { 
  Write-Host "last exit code: $LASTEXITCODE"
}
Finally {
  Write-Host "finally last exit code: $LASTEXITCODE"
  if ($result.FailedCount -le 0) {
    if ($env:APPVEYOR_JOB_ID) {
      Set-Content -Path $env:APPVEYOR_SPECTRA_TESTS_PASSSED_SENTINEL -Value $null
    }
  }
  if ($env:APPVEYOR_JOB_ID) {
    (New-Object "System.Net.WebClient").UploadFile("https://ci.appveyor.com/api/testresults/nunit/$($env:APPVEYOR_JOB_ID)", (Resolve-Path ".\testResults.xml"))
  }
} 
