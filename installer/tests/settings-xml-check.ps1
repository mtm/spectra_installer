param(
    [Parameter(Mandatory=$true)][string]$xmlpath,
    [Parameter(Mandatory=$false)][string]$testbasedir = "."
)

$testdir = Join-Path $testbasedir settings_test
$configpath = Join-Path $testdir tidy.config

if(Test-Path($testdir)) { Remove-Item -Recurse $testdir }

MkDir -Force $testdir | Out-Null
Set-Location $testdir
Copy-Item $xmlpath $testdir

@'
clean: yes
drop-proprietary-attributes: no
force-output: yes
indent-attributes: yes
indent-spaces: 2
indent: yes
input-xml: yes
output-xml: yes
quiet: yes
tidy-mark: no
vertical-space: false
wrap: 0
output-file: settings-tidy-out.xml
error-file: settings-tidy-err.txt
'@ | Out-File -Encoding ASCII $configpath

$status = &tidy -config tidy.config ($xmlpath | Split-Path -Leaf)
